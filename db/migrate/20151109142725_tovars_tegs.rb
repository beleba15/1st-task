class TovarsTegs < ActiveRecord::Migration
  def change
     create_table :tags_tovars, id: false do |t|
      t.integer :tovar_id
      t.integer :tag_id
     end
  end
end
