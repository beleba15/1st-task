class FilterTovar < ActiveRecord::Migration
  def change
     create_table :filters_tovars, id: false do |t|
      t.integer :tovar_id
      t.integer :filter_id
     end
  end
end
