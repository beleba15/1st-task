class CreateTovars < ActiveRecord::Migration
  def change
    create_table :tovars do |t|
      t.string :name
      t.integer :price
      t.integer :reprice
      t.text :desc
      t.string :url

      t.timestamps null: false
    end
  end
end
