class AddRemoveColumnTagsName < ActiveRecord::Migration
  def change
    rename_column :tags, :season, :name
  end
end
