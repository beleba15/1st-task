class Moderate < ActiveRecord::Migration
  def change
  	add_column :comments, :moderate, :boolean, default: false
  end
end
