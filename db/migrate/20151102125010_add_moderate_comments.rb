class AddModerateComments < ActiveRecord::Migration
  def change
	add_column :comments, :moderate, :boolean, null: false, :default => 0
  end
end
