class AddAttachmentArhitectImgToArchitectureImgs < ActiveRecord::Migration
  def self.up
    change_table :architecture_imgs do |t|
      t.attachment :arhitect_img
    end
  end

  def self.down
    remove_attachment :architecture_imgs, :arhitect_img
  end
end
