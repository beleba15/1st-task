class CreateArchitectureImgs < ActiveRecord::Migration
  def change
    create_table :architecture_imgs do |t|
      t.integer :architecture_id

      t.timestamps null: false
    end
  end
end
