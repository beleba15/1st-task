class AddAttachementImageToTovar < ActiveRecord::Migration
  def self.up
    change_table :tovars do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :tovars, :image
  end
end
