class AddRemoveTypePosition < ActiveRecord::Migration
  def change
     change_column :tovars, :position, :boolean, default: false
  end
end
