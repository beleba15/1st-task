class CreateFilterAttributes < ActiveRecord::Migration
  def change
    create_table :filter_attributes do |t|
      t.string :name
      t.string :description
      t.text :text
      t.timestamps null: false
    end
  end
end
