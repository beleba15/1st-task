class CreateNews < ActiveRecord::Migration
def change
    create_table :news do |t|
    t.string :title
    t.string :keywords
    t.text   :description
	t.string :name
	t.string :url
	t.text   :text
	t.boolean :status
    t.timestamps null: false
    end
  end
end
