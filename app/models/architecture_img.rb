class ArchitectureImg < ActiveRecord::Base
	belongs_to :tovar
	has_attached_file :arhitect_img, :styles => {:thumb => "800x504>", admin: "50x50>" }, default_url: "missing.png"
	validates_attachment_content_type :arhitect_img, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
