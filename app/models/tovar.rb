class Tovar < ActiveRecord::Base
  has_many :architecture_img
  accepts_nested_attributes_for :architecture_img, allow_destroy: true

  has_and_belongs_to_many :tag
  accepts_nested_attributes_for :tag, allow_destroy: true

  has_and_belongs_to_many :filter
  accepts_nested_attributes_for :tag, allow_destroy: true

  validates :name, :url, :image, presence: true
  validates :url, uniqueness: true
  validates_format_of :url, :with => /\A[А-Яа-яA-Za-z0-9\-\_]+\Z/

  has_attached_file :image, :styles => {:thumb => "1000x1000>", admin: "50x50>" }, default_url: "missing.png"
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  attr_writer :remove_image
  before_validation { self.image.clear if self.remove_image == '1' }

  def remove_image
    @remove_image || false
  end

end
