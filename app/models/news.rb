class News < ActiveRecord::Base
	validates :title, :keywords,  :description, :name, :url, presence: true
	validates :url, uniqueness: true
end
