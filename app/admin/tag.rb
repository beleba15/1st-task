ActiveAdmin.register Tag do
	menu  parent: "Под категории", label: "Метки"
	permit_params :id, :name
end
