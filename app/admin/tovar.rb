ActiveAdmin.register Tovar do
menu  parent: "Страницы", label: "Товар"
  permit_params :desc, :price, :reprice, :title, :text, :url, :name, :remove_image, :image, :position,
  :architecture_img_attributes => [:id, :_delete, :arhitect_img, :_destroy], :tag_attributes => [:id, :_delete, :name, :_destroy], :tag_ids => [], :filter_attributes => [:id, :_delete, :name, :_destroy], :filter_ids => []

  config.filters = false
  config.sort_order = 'position_asc'
  config.paginate   = false

  batch_action :position_true do |ids|
    Tovar.where(id: ids).update_all(position: true)
    redirect_to collection_path, notice: "Пост(и) виведені"
  end

  batch_action :position_false do |ids|
    Tovar.where(id: ids).update_all(position: false)
    redirect_to collection_path, notice: "Пост(и) зняті"
  end

  member_action :position_true, method: :get do 
    resource.update(position: true)
    redirect_to collection_path, notice: "Готово!"
  end

  sortable
  index do
    selectable_column
    column :position
    column "Название", :name
    column :url
    column :price
    column :reprice
    column "Картинка" do |img|
      image_tag img.image.url(:admin)
    end
    actions do |object|
      if object.position == false 
        link_to :position, position_true_admin_tovar_path(object)
      end
    end
  end

  form :html => {:multipart => true} do |f|
    f.inputs "Архитектура" do
      f.input :name, label: "Название"
      f.input :desc, :label => "Description"
      f.input :title, :label => "Размеры"
      f.input :text, :label => "Срок изготовления"
      f.input :price, :label => "Ціна"
      f.input :reprice, :label => "Нова ціна"
      f.input :url, :label => "Урл"
      f.input :image, :label => "Картинка", hint: (("Картинка:<br/>").html_safe +        f.template.image_tag(f.object.image.url(:thumb))).html_safe
      f.input :tag, input_html:{style: "width: calc(80% - 22px)"}
      f.input :filter, input_html:{style: "width: calc(80% - 22px)"}
    end

    f.inputs "Картинки" do
      f.has_many :architecture_img do |p|
        p.input :arhitect_img, :label => "Картинка", hint: (("Картинка:<br/>").html_safe + p.template.image_tag(p.object.arhitect_img.url(:thumb))).html_safe
        p.input :_destroy, :as => :boolean, required: false, label: "Удалить каритнку"
      end
    end
    f.actions
  end
end
