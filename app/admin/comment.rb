ActiveAdmin.register Comment, as: "User_comment" do

  permit_params :name, :email, :comment, :moderate


  batch_action :moderate_true do |ids|
    Comment.where(id: ids).update_all(moderate: true)
    redirect_to collection_path, notice: "Пост(и) виведені"
  end

  batch_action :moderate_false do |ids|
    Comment.where(id: ids).update_all(moderate: false)
    redirect_to collection_path, notice: "Пост(и) зняті"
  end

  member_action :moderate_true, method: :get do 
    resource.update(moderate: true)
    redirect_to collection_path, notice: "Готово!"
  end

  config.per_page = 10

  index do
    selectable_column
    column "І'мя", :name
    column "Майл", :email
    column "Коментарій", :comment
    column :moderate
    actions do |object|
    if object.moderate == false 
      link_to :moderate, moderate_true_admin_user_comment_path(object)
     end
    end
  end
end
