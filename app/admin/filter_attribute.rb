ActiveAdmin.register FilterAttribute do
   menu  parent: "Под категории", label: "Фільтер атрибути"
   permit_params :id, :name, :description, :text, :filter_id
end
