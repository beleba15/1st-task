ActiveAdmin.register News do
	menu  parent: "Страницы", label: "Новини"
	permit_params :title, :keywords, :description, :name, :url, :text, :status
end
