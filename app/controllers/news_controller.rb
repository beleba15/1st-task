class NewsController < ApplicationController
	def index
		@news = News.all.paginate(:page => params[:page], :per_page => 2)
	end
	def show
		@news = News.find_by_url(params[:url])
	end
end
