class CommentsController < ApplicationController
	def index
		@сomments = Comment.where(moderate: true)
	end
	def new
		@сomment = Comment.new
	end
	def create
		@сomment = Comment.new(comment_params)
		if @сomment.save
			redirect_to comments_new_path, notice: "Коментарій добавлено!"
		else
			flash.now.alert = "Помилка!"
			render :new
		end
	end
	private
	def comment_params
		params.require(:comment).permit(:name, :email, :comment, :moderate)
	end
end
