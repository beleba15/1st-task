class TovarController < ApplicationController
	def index
		@tovars = Tovar.all.where(position: true).order(position: :asc)
		@filter = Filter.all
	end
	def show
		@tovars = Tovar.find_by_url(params[:url])
	end
end
