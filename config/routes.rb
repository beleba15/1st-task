Rails.application.routes.draw do
	mount Ckeditor::Engine => '/ckeditor'
	devise_for :admin_users, ActiveAdmin::Devise.config
	ActiveAdmin.routes(self)
	root 'tovar#index'
	get 'news' =>  'news#index'
	get 'news/:url' =>  'news#show', as: 'news_show_url'
	get 'comments/new' =>  'comments#new'
	get 'comments' =>  'comments#index'
	post "comments" => "comments#create"
	get 'tovar/:url' =>  'tovar#show', as: 'tovar_url'
end
